#!/bin/bash

cd ${OTHER}
wget -c -nv --tries=2 https://apps.fs.usda.gov/fia/datamart/CSV/REF_POP_ATTRIBUTE.csv -O ./attributes_all.csv
wget -c -nv --tries=2 https://www2.census.gov/geo/tiger/GENZ2018/shp/cb_2018_us_county_20m.zip
wget -c -nv --tries=2 https://www2.census.gov/geo/tiger/GENZ2018/shp/cb_2018_us_state_20m.zip
wget -c -nv --tries=2 https://www2.census.gov/geo/docs/reference/state.txt

sleep 1
unzip -n ./cb*state*zip -d ./shape_state
unzip -n ./cb*county*zip -d ./shape_county

cat state.txt | awk -F '|' '{print $3","$2}' | tail -n +2 > ${PROJ_HOME}/state_abb.csv
cat state.txt | awk -F '|' '{print $2","$1}' | tail -n +2 > ${PROJ_HOME}/state_codes.csv

cd ${PROJ_HOME}
