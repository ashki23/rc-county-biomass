# Estimate US counties woody biomass availability and transportation cost

**REPOSITORY CITATION**

Mirzaee, Ashkan. Estimate US counties woody biomass availability and transportation cost (2021). https://doi.org/10.6084/m9.figshare.13923179

**RELATED PUBLICATION**

Picciano P, Aguilar FX, Burtraw D, Mirzaee A. Environmental and Socio-Economic Implications of Woody Biomass Co-firing at Coal-Fired Power Plants. Resource and Energy Economics. 2022 Feb 8:101296. https://doi.org/10.1016/j.reseneeco.2022.101296

## Access conditions
<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/3.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons Attribution-ShareAlike 3.0 Unported License</a>.
Sourcecode is available under a [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html).

## Contents
Bash and Python scripts to collect US counties biomass data from FIA database and computing cost of transportation from counties' centroid to other counties and coal burning power plants.

## Contact information
- Ashkan Mirzaee: amirzaee@mail.missouri.edu

## System requirement
The workflow in this repository is designed to run in both parallel and serial. To run this application in parallel you need a Linux based cluster with [slurm](https://slurm.schedmd.com/) job scheduling system. As a student, faculty or researcher, you might have access to your institute's cluster by using `ssh username@server.domain` on a Unix Shell (default terminal on Linux and macOS computers) or an SSH Client. If you do not have access to a cluster, the workflow can be run in serial on personal computers' Unix Shell. A basic knowledge about [Unix Shell](https://ashki23.github.io/shell.html) and [HPC](https://ashki23.github.io/hpc.html) can help to follow the workflow easily.

## Software
The following software and packages are required:
- Miniconda3
- Python > 3.8
   - openpyxl
   - xlrd
   - shapely 
   - fiona

`environment.sh` is designed to install the required software.

## Configurations
The following shows the project configurations:
```json
{
    "year" : [2018],
    "state": ["ALL"],
    "attribute_cd": [13,17,207,231],
    "tolerance": 1,
    "fixed_cost": 7.50,
    "incre_cost": 0.15,
    "job_number_max": 24,
    "job_time_hr": 8,
    "partition": "Lewis"
}
```

**Metadata** - `config.json` includes:
- **year:** a single year. Use a singleton list *e.g. [2017]*
- **state:** list of states. Use `["ALL"]` to include all states in the data. For a single state use a singleton list *e.g. ["MO"]*
- **attribute_cd:** list of FIA forest attributes code. For a single code use a singleton list *e.g. [7]*
- **tolerance:** a binary variable of 0 or 1. Set 1 to use the closest available (FIA survey year)[https://apps.fs.usda.gov/fia/datamart/recent_load_history.html] to the listed years, if the listed year is not available in FIA
- **fixed_cost:** fixed cost for biomass transportation per ton 
- **incre_cost:** incremental cost for biomass transportation per ton
- **job_number_max:** max number of jobs that can be submitted at the same time. Use 1 for running in serial
- **job_time_hr:** estimated hours that each job might takes (equivalent to `--time` slurm option). Not required for running in serial
- **partition:** name of the selected partition in the cluster (equivalent to `--partition` slurm option). Not required for running in serial

**Notes:**
- For serial running in personal computers set `"job_number_max": 1`. Otherwise, increasing number of jobs can submit more parallel jobs and reduce running time
- Change values in the `config.json` to run the application for different costs, years and states

## Data collection
To run this project we need to download several datasets from different resources. The following shows metadata and descriptions of the required datasets.

### Coal burning power plants
In our research we use a proprietary data collected by Resources for the Future. For the public repository, we collected information about coal burning power plants from US Energy Information Administration (EIA):
- [EIA-860](https://www.eia.gov/electricity/data/eia860/) - includes information about generators and power plants
- [EIA-923](https://www.eia.gov/electricity/data/eia923/) - includes annual MWh generation of power plants by fuel

We used EIA-860 *"Generator_Yr.xls"* to select coal burning power plants (**Table 1**); and used EIA-860 *"Plant_Yr.xls"* to find the power plants location. We also, used *"Generation and Fuel Data"* from EIA-923 to find total generation of selected power plants.

**Table 1**: Coal fuels

Code|Unit|Lower higher heating value (MMBtu)|Upper higher heating value (MMBtu)|Description
---|---|---|---|---
ANT|Tons|22|28|Anthracite Coal 
BIT|Tons|20|29|Bituminous Coal
LIG|Tons|10|14.5|Lignite Coal
SGC|Mcf|0.2|0.3|Coal-Derived Synthesis Gas
SUB|Tons|15|20|Subbituminous Coal
WC|tons|6.5|16|Waste/Other Coal (incl. anthracite culm, bituminous gob, fine coal, lignite waste, waste coal)
RC|tons|20|29|Refined Coal

**Metadata** - `power_info.py` generates a CSV file including:
- **plant_code:*** EIA power plant code
- **state:*** power plants state
- **lat:*** power plants latitude
- **lon:*** power plants longitude
- **nerc_region:** North American Electric Reliability Corporation ([NERC](https://www.nerc.com/AboutNERC/keyplayers/Pages/default.aspx)) regions
- **opened:** year that the first coal-fired generator started operating at each power plant
- **net_generation:** power plant annual generation (MWh)
- **year:*** year of the EIA data

**Note:** you can use your own coal-fired power plants dataset. The data should be a CSV file, named `power_info.csv`, and located in the main directory (`HOME_PROJ`). Your data can include different columns but marked (*) columns are required and should have the same header.

### FIA database
`prep_data.py` is generating a JSON output to feed `fia_county.py`. We used this Python scraping program to collect US counties' forest attributes from [FIA EVALIDator](https://apps.fs.usda.gov/Evalidator/evalidator.jsp). Forest Inventory and Analysis (FIA) data is downloaded for all US counties by using hundred parallel jobs to minimize download time. In this study, we collected level of the following timberland attributes:
- **13:** Aboveground biomass of live trees (at least 1 inch d.b.h./d.r.c), in dry short tons, on timberland 
- **17:** Net merchantable bole volume of live trees (at least 5 inches d.b.h./d.r.c.), in cubic feet, on timberland
- **207:** Average annual net growth of sound bole volume of trees (at least 5 inches d.b.h./d.r.c.), in cubic feet, on timberland
- **231:** Average annual removals of sound bole volume of trees (at least 5 inches d.b.h./d.r.c.), in cubic feet, on timberland

**Metadata** - the Forest Inventory and Analysis (FIA) description and user guide (version 8.0) is available [here](https://www.fia.fs.fed.us/library/database-documentation/current/ver80/FIADB%20User%20Guide%20P2_8-0.pdf). States codes (`state_codes.csv`) and FIA attributes are listed at B and O appendices respectively.

**Note:** FIA EVALIDator might not be available during maintaining and updating FIA databases. Please check [FIA User Alerts](https://www.fia.fs.fed.us/tools-data/) for more information.

## Woody biomass availability
Counties net annual woody biomass increase (NAWI) can be estimated by ([Goerndt et al. (2013)](https://doi.org/10.1016/j.biombioe.2013.08.032)):

```math
NAWI = \dfrac{Vol_g - Vol_r}{Vol_t} \times Bio
```
Where 
- $`Vol_g`$ average annual growth volume of timberland trees’ bole (att. 207)
- $`Vol_r`$ average annual removals volume of timberland trees’ bole (att. 231)
- $`Vol_t`$ net volume of timberland trees’ bole (att. 17)
- $`Bio`$ weight of aboveground biomass of timberland live trees (att. 13)

Above variables will be collected by `fia_county.py`.

## Transportation cost
The biomass transportation cost, also, is determined based on the distance from the supply county centroid to the coal facility such that: 

```math
T_{ij} = 7.5 + (0.15 \times d_{ij})
```

Where $`T_{ij}`$ is transportation cost (per ton) and $`d_{ij}`$ is distance from centroid of country $`i`$ to county/power plant $`j`$. A fixed transportation cost of `$7.5` per ton and an incremental transportation cost of `$0.15` per ton were applied to calculate biomass transportation cost ([Goerndt et al. (2013)](https://doi.org/10.1016/j.biombioe.2013.08.032)). 

`transport_cost.py` is calculating distance of both counties to counties centroid and counties centroid to coal-firing power plants. It also calculate corresponding costs.

## Workflow
The workflow includes the following steps:

- Setup the environment
- Download counties information by Bash
- Download power plants datasets by Python
- Power plants data preparation by Python
- Transportation cost calculation by Python
- Download FIA dataset and generate outputs by Python

You can find the workflow in `batch_file.sh`. Finally, to submit jobs and generate outputs run `sbatch batch_file.sh` in a cluster or `source batch_file.sh` in a Unix Shell.

---
<div align="center">
Copyright 2020-2021, [Ashkan Mirzaee](https://ashki23.github.io/index.html) | Content is available under [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/) | Sourcecode licensed under [GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.en.html)
</div>
