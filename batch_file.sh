#!/bin/bash

#SBATCH --job-name=Batch-Runner
#SBATCH --cpus-per-task=2
#SBATCH --mem=64G

echo ============ Local environments ============ $(hostname) $(date)

source environment.sh

echo ============ Checking config file ==========

if ! jq -e . config.json >/dev/null 2>&1; then
echo "Config JSON file is nat valid:"; jq . config.json
return || exit
fi

if [ `jq ."job_number_max" config.json` -gt 1 ]; then
if [ -z $(which sbatch) ]; then
echo "'sbatch' command not found. In 'config.json' assign 1 for 'job_number_max' to proceed in serial."
return || exit
fi
fi

echo ============ Download databases ============ $(hostname) $(date)

if [ ! -f ${PROJ_HOME}/power_info.csv ]; then
python power_info.py config.json || return || exit; else
echo "*** A 'power_info.csv' file has been found in 'PROJ_HOME'. If the current file is outdated, remove it and rerun the 'batch_file' ***"; fi
source download.sh
python prep_data.py config.json || return || exit
python transport_cost.py config.json

echo =============== HTML queries =============== $(hostname) $(date)

## Download forest attributes from FIA online source
python fia_county.py config.json attributes.json
