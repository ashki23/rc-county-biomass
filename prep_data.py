#!/usr/bin/env python

import sys
import csv
import json
import xlrd
import openpyxl
import collections

def csv_dict(csv_file):
    csv_data = csv.reader(csv_file)
    dict_data = {}
    for row in csv_data:
        dict_data[row[0]] = ','.join([str(x) for x in row[1:]])
    return dict_data

def csv_list_tuple(csv_file):
    csv_data = csv.reader(csv_file)
    list_data = []
    for row in csv_data:
        list_data.append(tuple(row))
    return list_data

def csv_list_dict(csv_file):
    csv_data = csv.reader(csv_file)
    header = next(csv_data)
    assert all([isinstance(x, str) for x in header]), "Header (first row) includes not-string elemnets"
    header = [x.lower() for x in header]
    ncol = len(header)
    list_dict = []
    for row in csv_data:
        if len(row) == ncol:
            mm = {}
            for i in range(ncol):
                mm[header[i]] = row[i]
            list_dict.append(mm)
    return list_dict

def select_state_config(list_dict,config):
    if config['state'] == ['ALL']:
        return list_dict
    else:
        select = []
        for k in list_dict:
            if k['state'] in config['state']:
                select.append(k)
        return select

def state_config(dict_file,config):
    if config['state'] == ['ALL']:
        return dict_file
    else:
        select = dict_file.copy()
        for scd in dict_file:
            if scd not in config['state']:
                del select[scd]
        return select

def select_att_config(dict_,config):
    select = {}
    for i in config['attribute_cd']:
        select[str(i)] = dict_[str(i)]
    return select

def select_uniq_id(list_dict,id_name):
    id_list = []
    for k in list_dict:
        id_list.append(k[id_name])
    id_list = list(set(id_list))
    uniq = []
    for j in list_dict:
        if j[id_name] in id_list:
            uniq.append(j)
            id_list.remove(j[id_name])
    return uniq

def dict_csv(dict_data,csv_output):
    fcsv = csv.writer(csv_output, lineterminator = '\n')
    for j in dict_data:
        row = [j] + [dict_data[j]]
        fcsv.writerow(row)

def list_dict_csv(list_dict,keys,csv_output):
    fcsv = csv.writer(csv_output, lineterminator = '\n')
    fcsv.writerow(keys)
    for i in list_dict:
        for j in keys:
            if j in i.keys():
                if type(i[j]) is list:
                    i[j] = ','.join(i[j])
            else:
                i[j] = 'NA'
        row = [i[x] for x in keys]
        fcsv.writerow(row)

def list_dict_panel(list_dict,keys,config,csv_output):
    fcsv = csv.writer(csv_output, lineterminator = '\n')
    fcsv.writerow(keys + ['year'] + ['att_%s' % x for x in config['attribute_cd']])
    for i in list_dict:
        for j in keys:
            if j in i.keys():
                if type(i[j]) is list:
                    i[j] = ','.join(i[j])
            else:
                i[j] = 'NA'
        row = [i[x] for x in keys]
        for y in config['year']:
            att_yr = []
            att_yr.extend(['%s_%s' % (x,y) for x in config['attribute_cd']])
            for ay in att_yr:
                if ay not in i.keys():
                    i[ay] = 'NA'
            fcsv.writerow(row + [y] + [i[x] for x in att_yr])

def xl_csv(xl_file,csv_output):
    fcsv = csv.writer(csv_output, lineterminator = '\n')
    if "xlsx" in str(xl_file):
        wb = openpyxl.load_workbook(xl_file)
        for x in wb.sheetnames:
            ws = wb[x]
            fcsv.writerow("sheet name: %s" % x)
            for row_cells in ws.iter_rows():
                fcsv.writerow([cell.value for cell in row_cells])
    else:
        wb = xlrd.open_workbook(xl_file)
        for x in wb.sheet_names():
            ws = wb.sheet_by_name(x)
            fcsv.writerow("sheet name: %s" % x)
            for rowx in range(ws.nrows):
                row_ = ws.row_values(rowx)
                fcsv.writerow(row_)

def xl_sheet_csv(xl_file,sheet_name,csv_output):
    fcsv = csv.writer(csv_output, lineterminator = '\n')
    if "xlsx" in str(xl_file):
        wb = openpyxl.load_workbook(xl_file)
        ws = wb[sheet_name]
        for row_cells in ws.iter_rows():
            fcsv.writerow([cell.value for cell in row_cells])
    else:
        wb = xlrd.open_workbook(xl_file)
        ws = wb.sheet_by_name(sheet_name)
        for rowx in range(ws.nrows):
            row_ = ws.row_values(rowx)
            fcsv.writerow(row_)

def xl_sheetnames(xl_file):
    if "xlsx" in str(xl_file):
        wb =  openpyxl.load_workbook(xl_file)
        return wb.sheetnames
    else:
        wb = xlrd.open_workbook(xl_file)
        return wb.sheet_names()

if __name__=='__main__':
    ## Read config
    config = json.load(open(sys.argv[1]))
    year = config['year']
    
    ## FIA attributes
    with open('./other_data/attributes_all.csv', 'r') as att:
        cd_att_all = csv_list_dict(att)
    
    ## FIA attributes number and name
    cd_att = {}
    for atr in cd_att_all:
        cd_att[atr['attribute_nbr']] = atr['attribute_descr']
    
    ## JSON and CSV outputs of selected FIA attributes (number and name)
    att_select = select_att_config(cd_att,config)
    with open('attributes.json', 'w') as atj:
        json.dump(att_select, atj)
