#!/bin/bash

export PROJ_HOME=${PWD}
export OUTPUT=${PWD}/outputs
export OTHER=${PWD}/other_data
export FIA=${PWD}/fia_data
install -dvp ${OUTPUT}
install -dvp ${OTHER}/power_plants
install -dvp ${FIA}/survey
MYHOME=${HOME}

## Install Miniconda
if [ ! -d miniconda ]; then 
source bootstrap.sh
fi

## Initiate conda
export HOME=${PROJ_HOME}
export PATH=${PROJ_HOME}/miniconda/bin/:${PATH}
source ./miniconda/etc/profile.d/conda.sh

## Deactivat active envs
conda deactivate

echo ============ Create Conda envs ============= $(hostname) $(date) 

## Create local environment
if [ ! -d cofire_py_env ]; then
## Including: python openpyxl xlrd shapely fiona
conda create --yes --prefix ./cofire_py_env --file ./cofire_py_env.txt
fi

## Activate the local env
conda activate ./cofire_py_env
export HOME=${MYHOME}
