#!/usr/bin/env python

import sys
import csv
import math
import json
import time
import glob
import fiona
import shapely
import prep_data
from shapely.geometry import Point
from shapely.geometry import Polygon

def csv_coord_point(csv_file):
    coord_data = csv.reader(csv_file)
    header = next(coord_data)
    assert all([isinstance(x, str) for x in header]), "\nHeader (first row) includes not-string elemnets"
    header = [x.lower() for x in header]
    assert 'lat' and 'lon' and 'plant_code' in header, "\nHeader (first row) does not include 'lat', 'lon' or 'plant_code' elements.\nIf these elements available with different names, you should rename them accordingly.\nNote that 'plant_code' is a unique id for each power plant (node)"
    ncol = len(header)
    list_dict = []
    for row in coord_data:
        mm = {}
        for i in range(ncol):
            mm[header[i]] = row[i]
        coord = (float(mm['lon']),float(mm['lat']))
        #mm['point'] = shapely.geometry.Point(coord) # As Point
        mm['point'] = coord # As tuple
        list_dict.append(mm)
    return list_dict

def cent_shape_polyg(shape_file):
    geo_cent = {}
    for sf in shape_file:
        geo_id = sf['properties']['GEOID']
        polyg = shapely.geometry.shape(sf['geometry'])
        cent_point = polyg.centroid # As Point
        cent_coord = (cent_point.x,cent_point.y) # As tuple
        geo_cent[geo_id] = cent_coord
        # TEST
        #if geo_id in ['01027','31039']:
        #    print(geo_id,cent_coord)
    return geo_cent

def haversine(point1, point2): # points = (x,y) = (lon,lat)
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(math.radians,[point1[0], point1[1], point2[0], point2[1]])

    # haversine formula 
    dlon = lon2 - lon1
    dlat = lat2 - lat1 
    a = math.sin(dlat/2)**2 + math.cos(lat1) * math.cos(lat2) * math.sin(dlon/2)**2
    c = 2 * math.asin(math.sqrt(a)) 
    r = 6371 # Radius of earth in kilometers. Use 3956 for miles
    return c * r

if __name__=='__main__':    
    ## Read config
    config = json.load(open(sys.argv[1]))
    
    ## Create a dictionary including power plants coordinates
    with open('./power_info.csv') as po:
        power_data = csv_coord_point(po)
    
    power_data = prep_data.select_uniq_id(power_data,'plant_code')
    print("Length of input data:", len(power_data))
    
    ## Select states from config
    power_data = prep_data.select_state_config(power_data,config)
    
    ## Read the shape file and find the centroid
    with fiona.open(glob.glob('./other_data/shape_county/*county*.shp').pop()) as src:
        cent_data = cent_shape_polyg(src)
    
    ## Woodybiomass transportation ($/Megagrams) county-plant CSV output
    time = time.strftime("%Y%h%d-%H%M")
    fix_cost = config['fixed_cost']
    inc_cost = config['incre_cost']
    with open('./outputs/transport-cost-%s.csv' % time, 'w') as f:
        fcsv = csv.writer(f)
        header = ['county(row)_to_powerPlant(col)_cost_per_dollar_per_Mg'] + [x['plant_code'] for x in power_data]
        fcsv.writerow(header)
        for i in cent_data:
            row = [i] + [round(fix_cost + inc_cost * haversine(x['point'],cent_data[i]),4) for x in power_data]
            fcsv.writerow(row)

    ## Woodybiomass transportation ($/Megagrams) county-county CSV output
    with open('./outputs/countytocounty-tran-cost-%s.csv' % time, 'w') as f:
        fcsv = csv.writer(f)
        header = ['county(row)_to_county(col)_cost_per_dollar_per_Mg'] + [x for x in list(cent_data.keys())]
        fcsv.writerow(header)
        for i in cent_data:
            row = [i] + [round(fix_cost + inc_cost * haversine(cent_data[x],cent_data[i]),4) for x in cent_data]
            fcsv.writerow(row)
